'use strict';

angular.module('diaryApp', [
  'angular-loading-bar',
  'angular-jwt',
  'angular-storage'
])

.constant('URL', {
    getToken: "http://fd-api-2-testing.freelancediary.com/auth",
    validedToken: "http://fd-api-2-testing.freelancediary.com/auth/test_fdjwtv1"
})
.constant('USER', {
    "applicationId": "jr6V8KpEWkDqdXaqFWBmxhtbbXwJsbwscFIOSreI0MM=",
    "deviceUUID":  "566b7c6a-8130-4e96-bf0a-6c1950f2fa46",
    "deviceName":  navigator.userAgent
})

// .config(function ($httpProvider) {
//     $httpProvider.interceptors.push('AuthInterceptor');
// })

.config(function Config($httpProvider, jwtInterceptorProvider) {
    jwtInterceptorProvider.tokenGetter = function($http, jwtHelper, AuthToken, URL, USER) {
        var credentials = localStorage.getItem('credentials');
        var token = (credentials) ? JSON.parse(credentials).jwtToken : null;
        var accessToken = (credentials) ? JSON.parse(credentials).accessToken : null;
        var refreshToken = (credentials) ? JSON.parse(credentials).refreshToken : null;

        if (jwtHelper.isTokenExpired(token) || AuthToken.refresh) {
          // This is a promise of a JWT id_token
          console.log('expired!')
          return $http({
            url: URL.getToken + "/refresh",
            skipAuthorization: true,
            method: 'POST',
            data: {
                accessToken: accessToken,
                refreshToken: refreshToken
            }

        }).then(function(response) {
            console.log('successs ', response.headers("Authorization"));

            var credentials = response.data;
            credentials.jwtToken = response.headers("Authorization");
            AuthToken.setCredentials(credentials);

            return token;
        });
        } else {
            return token;
        }
    }

  $httpProvider.interceptors.push('jwtInterceptor');
})

.factory('AuthToken', function($window) {
    var credentialKey = 'credentials';
    var storage = $window.localStorage;
    var cachedToken;
    return {
        setCredentials: setCredentials,
        getCredentials: getCredentials,
        refresh: refresh
        // isAuthenticated: isAuthenticated,
        // clearCredentials: clearCredentials
    };
    function setCredentials(token) {
        cachedToken = token;
        storage.setItem(credentialKey, JSON.stringify(token));
        // storage.setItem(credentialKey, token);
    }

    function getCredentials() {
        if (!cachedToken) {
            cachedToken = (storage.getItem(credentialKey)) ? JSON.parse(storage.getItem(credentialKey)) : null;
        }
        return cachedToken;
    }

    function refresh(bool) {
        return bool;
    }
})

.service('uuid', function() {
    var fmt = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
    this.generate = function() {
        return fmt.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    };
})


.controller('diaryCtrl', function ($scope, $window, $http, $timeout, AuthToken, uuid, URL, USER) {
    $scope.desc = 'JSON Web Token based authentication';
    
    $scope.authenticate = function(refresh){
        AuthToken.refresh = refresh;

        $http({
            url: URL.getToken,
            method: 'POST',
            headers: {
               'X-Reference': uuid.generate(),
               'X-UTC-Timestamp': new Date().getTime()
            },
            data: USER
        })

        .then(function success(response) {
            var credentials = response.data;
            credentials.jwtToken = response.headers("Authorization");
            AuthToken.setCredentials(credentials);

            $scope.response = AuthToken.getCredentials();

        }, function error(response) {
            if (response.status === 404) {
                $scope.error = 'Error! Wrong logins details.';
            } else {
                $scope.error = 'Error! Problem logging in.';
            }
        });
    }

    $scope.validedToken = function(refresh){
        AuthToken.refresh = refresh;
        $http({
            url: URL.validedToken,
            method: 'GET',
            responseType: "json",
        })
        .then(function success(response) {
            $scope.status = response.status;
            $scope.statusText = response.statusText;

            console.log('Success ', response);

        }, function error(response) {
            console.log('Error ', response);
        });
    }
})


;